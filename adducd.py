#!/usr/bin/env python

from astropy.io import fits
import json
import os


def fitsheader(inputfile):
    jsonFile = open(os.path.dirname(os.path.abspath(__file__))+"/map.json")
    jsonData = json.loads(jsonFile.read())
    ucdMap = jsonData['ucdmap']
    unitMap = jsonData['unitmap']

    ncols = inputfile[1].header['TFIELDS']
    for n in range(ncols):
        fieldname = inputfile[1].header['TTYPE{}'.format(n+1)]
        ucdname = 'TUCD{}'.format(n+1)
        unitname = 'TUNIT{}'.format(n+1)
        if ucdname in inputfile[1].header:
            del inputfile[1].header[ucdname]
        if unitname in inputfile[1].header:
            del inputfile[1].header[unitname]
        if fieldname in ucdMap:
            inputfile[1].header[ucdname] = ucdMap[fieldname]
            inputfile[1].header[unitname] = unitMap[fieldname]
        else:
            inputfile[1].header[ucdname] = ""
            inputfile[1].header[unitname] = ""


if __name__ == "__main__":
    import sys
    inputfile = fits.open(sys.argv[1], mode='update')
    if 'TUNIT1' not in inputfile[1].header:
        fitsheader(inputfile)
        # for key in inputfile[1].header:
        #    if 'HISTORY' not in key:
        #            print("{} {}".format(key, inputfile[1].header[key]))
        inputfile.flush()
        inputfile.close()
    else:
        print("Header already complete")
