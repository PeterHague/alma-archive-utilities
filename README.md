# README #

Programs that I have found useful in processing products from the ALMA archive https://almascience.eso.org/aq/

At present there is only one program

* metadata.py - adds a PROJID field to the header of .fits products that contains the project id in the format 20xx.y.zzzzz